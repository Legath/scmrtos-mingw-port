#include "platform.h"
#include <QDebug>
#include <scmRTOS.h>
#include <QTime>
#include <math.h>

#define private public
#include <QThread>

#include <windows.h>
#include <process.h>

#define SYSTICK_INTERVAL ( 1000 / SYSTICK_FREQ )




TSensorProc SensorProc;
TGSMProc    GSMProc;
TSolver     Solver;

HANDLE CPlatform :: thread;

namespace OS
{

template <>
OS_PROCESS long WINAPI TSensorProc::exec( void* )
{
    long long n = 0;
    for(;;)
    {
        ++n;
        qDebug() << "TSensorProc " << n++ << "sys: " << CPlatform :: SysTick;
        OS :: sleep( 100 );
        if( CPlatform :: SysTick % 1000 == 0 )
            qDebug() << "TSensorProc " << n << "sys: " << CPlatform :: SysTick;
    }
}
}
namespace OS
{
/** --------------------------------------------------------------------
 * Процесс работы с модемом
 */
template <>
OS_PROCESS long WINAPI TGSMProc :: exec( void* )
{
    for( ;; )
    {
         qDebug() << "GSM";
         OS:: sleep( 1000 );
    }
}
}

namespace OS
{
/** --------------------------------------------------------------------
 * Процесс принимающий решение
 */
template <>
OS_PROCESS long WINAPI TSolver :: exec( void* )
{
    for( ;; )
    {
        qDebug() << "Solver";
        OS:: sleep( 1500 );
    }
}
}





/** --------------------------------------------------------------------
 */
long CPlatform :: SysTick = 0;

/** --------------------------------------------------------------------
 */
void OS::system_timer_user_hook()
{
    ++CPlatform :: SysTick;
//    static int n = 0;
//    qDebug() << "system_timer_user_hook " << n++;
//    SuspendThread( OS::TKernel :: ProcessTable[ n & 1 ]->thread );
//    ResumeThread( OS::TKernel :: ProcessTable[ ~n & 1 ]->thread );

}

#if scmRTOS_IDLE_HOOK_ENABLE == 1
void OS::idle_process_user_hook()
{
    //static int n = 0;
    //qDebug() << "idle_process_user_hook  " << n++;
    Sleep( 1 );
}
#endif

/** ------------------------------------------------------------------------
 * @brief Запуск ядра
 */
void CPlatform :: startCore( )
{
    // Повышаем приоритет текущего потока
    HANDLE pvHandle = GetCurrentThread();
    SetThreadPriority( pvHandle, THREAD_PRIORITY_ABOVE_NORMAL );
    SetThreadPriorityBoost( pvHandle, TRUE );
    SetThreadAffinityMask( pvHandle, 0x01 );
    // Имитация системных часов
    connect( &tmrSysTick, SIGNAL( timeout( ) ) , this , SLOT( slotSysTick( ) ) );
    tmrSysTick.start( SYSTICK_INTERVAL );
    // Запускаем первый поток
    OS::TKernel :: ProcessTable[ OS :: pr0 ]->f_suspend = false;
    ResumeThread( OS::TKernel :: ProcessTable[ OS :: pr0 ]->thread );
}

/** --------------------------------------------------------------------------
 * @brief Имитация программного прерывания
 */
void CPlatform :: slotSys() { OS::Kernel.raise_context_switch(); }
/** --------------------------------------------------------------------------
 * @brief Имитирует работы системных часов
 */
void CPlatform :: slotSysTick( ) { OS::SysTick_Handler(); }
