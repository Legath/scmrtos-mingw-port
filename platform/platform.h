#ifndef _PLATFORM_H
#define _PLATFORM_H

#include <QObject>
#include <qtimer.h>
#include <windows.h>


class CPlatform : public QObject
{
    Q_OBJECT
signals:

public:
    void startCore( );
    static HANDLE thread;
    static long SysTick;
public slots:

private:
    QTimer  tmrSysTick;

private slots:
    void slotSysTick( );
    void slotSys( );
};


#endif
