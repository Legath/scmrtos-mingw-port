/*
 * process.h
 *
 *  Created on: 05 ����. 2014 �.
 *      Author: SkySpark
 */

#ifndef PROCESS_H_
#define PROCESS_H_

#include <io_system.h>
#include <scmRTOS.h>

/** --------------------------------------------------------------------
 * ����������� ���������
 */
// ���������� �� ��������: pr0 - pr1 - pr2 - ... - idle

/** --------------------------------------------------------------------
 * ������� ��������� �������� � ��������
 */
typedef OS::process< OS :: pr0, PROC_SENS_STACK > TSensorProc;
/** --------------------------------------------------------------------
 * ������� ������ � �������
 */
typedef OS::process< OS :: pr1, PROC_GSM_STACK > TGSMProc;
/** --------------------------------------------------------------------
 * ������� ����������� �������
 */
typedef OS::process< OS :: pr2, PROC_SOLV_STACK > TSolver;
///** --------------------------------------------------------------------
// * ������� ���������
// */
//typedef OS::process< OS :: pr3, PROC_OXI_STACK  > TOxiMeter;
///** --------------------------------------------------------------------
// * ������� ����������� ������
// */
//typedef OS::process< OS :: pr4, PROC_SERCON_STACK  > TSerCon;
///** --------------------------------------------------------------------
// * ������� GPS-���������
// */
//typedef OS::process< OS :: pr5, PROC_GPS_STACK     > TGPSProc;


#endif /* PROCESS_H_ */
