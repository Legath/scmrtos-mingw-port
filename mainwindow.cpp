#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

struct myParam
{

    int data1 = 1000;
    int data2 = 100;
    int max = [&]( int i ){ qDebug()<< "test"; return ( data1 > data2 ) ? data1 : data2; }( 12 );
};


MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    platform = new CPlatform;
}

MainWindow::~MainWindow()
{
    delete ui;
}

myParam PARAMS;

void MainWindow::on_pbStart_clicked()
{
    qDebug() << PARAMS.max;
    //PARAMS.max = 1000;
    PARAMS.data1 = 512;
    PARAMS.data2 = 1024;

    qDebug() << PARAMS.max;

    platform->startCore();
}
