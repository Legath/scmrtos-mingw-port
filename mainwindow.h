#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <platform.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pbStart_clicked();

private:
    Ui::MainWindow *ui;
    CPlatform*      platform;
};

#endif // MAINWINDOW_H
